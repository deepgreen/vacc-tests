#!/bin/env python

from selenium import webdriver

browser = webdriver.Firefox()
browser.get('https://dev-ondemand.uvm.edu/')

print(f"browser.title is {browser.title}")

print("here are the things you can do with a browser:")
print(dir(browser))

