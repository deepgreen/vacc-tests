#!/bin/env python

from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.support import expected_conditions as EC


import time
import sys
import selenium
import traceback

import argparse
import logging
import logging.config
import yaml
import csv
import re

class TimeoutException(Exception):
    """
        An exception to note when we've run into a timeout of our own design.
    """

logging.config.dictConfig(yaml.safe_load(open('logging.yaml')))
log = logging.getLogger(name="test2")

ap = argparse.ArgumentParser(description="OOD Jupyter workbook test.")
ap.add_argument('-u', metavar="USERNUM", type=int, help="Which user from the userlist should be used for this test")
ap.add_argument('-s', action='store_true', help="Take screenshots.")
ap.add_argument('-t', metavar="TAG", type=str, help="Tag for generated filenames, e.g. screenshots.")
args = ap.parse_args()

username=""
password=""

if (args.u):
    log.debug(f"I want to be user number {args.u}")
    userfilename="passwords.list"
    try:
        userfile = open(userfilename)
    except Exception as e:
        log.error(f"Could not open {userfilename}, {e}")
    userlist = list(csv.reader(userfile))
    (username, password) = userlist[args.u]
    log.debug(f"That is user {username}")
else:
    username = 'jtlawson'
    passwordFile = open('jtlawson-pw', 'r')
    password = passwordFile.read()

    if (password[-1] == '\n'):
        # chop off newline
        password = password[:-1]


#print (f"username: {username} password: {password}")


def waitforStringInCSSSel(browser, cssSel, string, timeout):
    """
       Loop waiting for string to appear in the element selected by cssSel.
       The element must already exist, otherwise we'll get an exception.
       If it doesn't appear within the timeout, raise TimeoutException.
    """

    label = browser.find_element_by_css_selector(cssSel)
    completed = False
    count = 0
    while(not completed):
            if string in label.text:
                    completed = True
            else:
                    time.sleep(1)
                    count = count+1
            if (count > timeout):
                    browser.get_screenshot_as_file(f"tmout_{args.u}_{args.t}.png")
                    raise TimeoutException(f"Timeout while waiting for \"{string}\"")

def waitforStringInHterm(browser, htermCSSSel, searchString, timeout):
    """
        Loop waiting for string to appear in any of the child elements of the element selected by cssSel.
        The element must exist, otherwise we'll get an exception.
       If the string doesn't appear within the timeout, raise TimeoutException.
    """

    log.info("starting waitforStringInHterm")

    htermElm = browser.find_element_by_css_selector( htermCSSSel  )

    if (not htermElm):
        raise Exception(f"Could not from hterm given CSS selector {htermCSSSel}")

    rowsCSSSel = htermCSSSel + " x-row"
    
    completed = False
    count = 0
    while(not completed):
        rows = browser.find_elements_by_css_selector(rowsCSSSel)
        for r in rows:
            #log.info(f"found row {r}")
            #log.info(f"dir of row: {dir(r)}")
            #log.info(f"row's text: {r.text}")
            if (searchString in r.text):
                completed = True
                break
        if (not completed):
            time.sleep(1)
            count = count+1
        if (count > timeout):
            browser.get_screenshot_as_file(f"tmout_{args.u}_{args.t}.png")
            raise TimeoutException(f"Timeout while waiting for string \"{searchString}\"")

def switchToNewWindowHandle(browser, oldWindowHandles):
    for handle in browser.window_handles:
        if handle not in oldWindowHandles:
            log.debug(f"switching to window handle {handle}")
            browser.switch_to_window(handle)
            return handle

def userLogin(browser, username, password, args):

    log.info(f"userLogin: browser.title is {browser.title}, user {args.u}")
    if ("WebAuth" in browser.title):
        user_input = browser.find_element_by_css_selector('input.username')
        pass_input = browser.find_element_by_css_selector('input.password')
        user_input.send_keys(username)
        pass_input.send_keys(password)
        sub_button = browser.find_element_by_css_selector('button.submit')
        sub_button.click()

    # wait for dashboard

    WebDriverWait(browser, 40).until(
        EC.title_contains("Dashboard"))

def OODTest(browser, username, password, args):
    log.info("starting OODTest")

    startMonoTime = time.monotonic()
    browser.get('https://vacc-ondemand.uvm.edu')

    userLogin(browser, username, password, args)

    # save first window handle, it should be the dashboard
    dashWindowHandle = browser.window_handles[0]
    oldWindowHandles = browser.window_handles

    WebDriverWait(browser, 20).until(
            EC.presence_of_element_located((By.PARTIAL_LINK_TEXT, "Clusters")))

    if("Dashboard" in browser.title):
        clustButton = browser.find_element_by_partial_link_text("Clusters")
        clustButton.click()
    else:
        raise Exception(f"Could not find dashboard - user {args.u}")

    WebDriverWait(browser, 20).until(
            EC.presence_of_element_located((By.PARTIAL_LINK_TEXT, "Shell Access")))

    browser.find_element_by_partial_link_text("Shell Access").click()

    WebDriverWait(browser, 20).until(
        EC.new_window_is_opened(oldWindowHandles))
    JNWinHandle = switchToNewWindowHandle(browser, oldWindowHandles)

    WebDriverWait(browser, 10).until(
        EC.title_contains("vacc-user2"))

    log.debug("I see vacc-user2 in the title")

    # Wait for "vacc-user2" prompt to appear in hterm window
    # %^#$* hterm is inside an iframe.  Hopefully at frame 0?

    browser.switch_to_frame(0)

    htermCSSSel = "#hterm\\3A row-nodes"

    WebDriverWait(browser, 10).until(
        EC.presence_of_element_located((By.CSS_SELECTOR, htermCSSSel)))

    log.debug(f"I see the hterm (user {args.u})")

    htermRowCSSSel = "#hterm\:row-nodes > x-row:nth-child(6)"

    WebDriverWait(browser, 20).until(
        EC.presence_of_element_located((By.CSS_SELECTOR, htermRowCSSSel)))

    log.debug("I see the hterm row")

    waitforStringInHterm(browser, htermCSSSel, "vacc-user2", 10)

    log.info("I see the term window and prompt")
    htermDiv = browser.find_element_by_css_selector('x-screen')
    htermDiv.send_keys('hostname | tr etaoin shrdlu')
    htermDiv.send_keys(Keys.ENTER)

    expectedStr = "vrcc-ussr2.clushsr"
    row7sel = "#hterm\:row-nodes > x-row:nth-child(7)"

    waitforStringInHterm(browser, htermCSSSel, expectedStr, 10)
    log.info(f"I see the expected output from hostname.  Shell is good. (user {args.u})")


def notebookTest(browser, username, password, args):
    log.info("starting notebookTest")

    browser = webdriver.Firefox()

    startMonoTime = time.monotonic()

    browser.get('https://vacc-ondemand.uvm.edu')

    userLogin(browser, username, password, args)

    WebDriverWait(browser, 20).until(
        EC.presence_of_element_located((By.PARTIAL_LINK_TEXT, "Interactive Apps")))

    # save first window handle, it should be the Interactive Apps one, for later use (delete button)
    iaWindowHandle = browser.window_handles[0]

    if ("Dashboard" in browser.title):
        iabutton = browser.find_element_by_partial_link_text("Interactive Apps")
        iabutton.click()
        # if we're lucky, we can find the "DeepGreen Jupyter Notebook" a element
        djnbutton = browser.find_element_by_partial_link_text("Jupyter Notebook")
        djnbutton.click()
        time.sleep(2)
    else:
        raise Exception(f"Could not find dashboard - user {args.u}")

    WebDriverWait(browser, 40).until(
        EC.title_contains("Jupyter Notebook"))

    assert("Jupyter Notebook" in browser.title)


    launchButton = browser.find_element_by_css_selector('input[name=commit]')
    launchButton.click()

    # It's been launched, now wait for "Starting/Queued/Running"
    cssSelToSessionStat = "div.card-heading h5.card-header div.float-right span.card-text"
    log.info(f"about to wait for job starting/queued/running - user {args.u}")
    WebDriverWait(browser, 40).until(
        EC.presence_of_element_located((By.CSS_SELECTOR, cssSelToSessionStat)))
    
    startingDiv = browser.find_element_by_css_selector(cssSelToSessionStat)
    log.debug(f"startingDiv.text: {startingDiv.text}, user {args.u}")

    log.debug(f"now wait for just Running, user {args.u}")
    

    running = 0
    i=0
    maxloop = 120

    while (running==0 and i < maxloop):
        i=i+1
        WebDriverWait(browser, 1).until(
            EC.presence_of_element_located((By.CSS_SELECTOR, cssSelToSessionStat)))
        startingDiv = browser.find_element_by_css_selector(cssSelToSessionStat)
        #print("startingDiv.text is ", startingDiv.text)
        if ("Running" in startingDiv.text):
                running = 1
        else:
                time.sleep(1)

    if (running==0):
        raise TimeoutException(f"Timed out waiting for session to be Running, user {args.u}.")
    else:
        log.info(f"Yay, session is running, user {args.u}.")

    # find the "Connect to Jupyter" button
    btn = browser.find_element_by_css_selector("button.btn-primary")
    assert("Connect to Jupyter" in btn.text)
    oldWindowHandles = browser.window_handles
    btn.click()
    WebDriverWait(browser, 20).until(
        EC.new_window_is_opened(oldWindowHandles))
    JNWinHandle = switchToNewWindowHandle(browser, oldWindowHandles)
    # wait for the jupyter notebook to come up
    log.debug("waiting for div.dynamic-instructions")
    WebDriverWait(browser,40).until(
        EC.presence_of_element_located((By.CSS_SELECTOR, "div.dynamic-instructions")))
    time.sleep(1)
    instr = browser.find_element_by_css_selector("div.dynamic-instructions")
    log.debug(f"jupyter instructions are: {instr.text}")


    # First we must clone the repo
    # Launch a python2 notebook
    # Click "New..."
    newBtn = browser.find_element_by_css_selector("button.dropdown-toggle:nth-child(1)");
    if (not newBtn):
            raise Exception("Could not find new button.");
    WebDriverWait(browser, 20).until(EC.visibility_of(newBtn))
    buttonFailed = 0
    buttonFailedExc = None;
    try:
        newBtn.click()
    except Exception as e:
        buttonFailed = 1
        buttonFailedExc = e
    if buttonFailed and args.s:
        browser.get_screenshot_as_file(f"new_notebook_clickfailed_user{args.u}_{args.t}.png")
        raise buttonFailedExc
        
    oldWindowHandles = browser.window_handles
    link="Python 3"
    WebDriverWait(browser, 10).until(
        EC.presence_of_element_located((By.PARTIAL_LINK_TEXT, link)))
    browser.find_element_by_partial_link_text(link).click()
    WebDriverWait(browser, 20).until(
        EC.new_window_is_opened(oldWindowHandles))
    time.sleep(1)
    switchToNewWindowHandle(browser, oldWindowHandles)

    # Find cell 1, where we can enter the git clone.
    cssSel = ".CodeMirror > div:nth-child(1) > textarea:nth-child(1)"
    WebDriverWait(browser, 20).until(
        EC.presence_of_element_located((By.CSS_SELECTOR, cssSel)))
    code_input = browser.find_element_by_css_selector(cssSel)
    
    ActionChains(browser).move_to_element(code_input).click().send_keys("!git clone https://gitlab.uvm.edu/deepgreen/workshop-ai.git workshopai").key_down(Keys.SHIFT).send_keys(Keys.ENTER).key_up(Keys.SHIFT).perform()

    # find the output box for that command
    cssSel = ".output_subarea > pre:nth-child(1)"
    WebDriverWait(browser,40).until(
            EC.presence_of_element_located((By.CSS_SELECTOR, cssSel)))
    waitforStringInCSSSel(browser, cssSel, "Unpacking objects:", 20)
    output_cell = browser.find_element_by_css_selector(cssSel)
    time.sleep(1)
    m = re.search(r"Unpacking objects: .*, done\.", output_cell.text)
    if m is None:
            raise Exception("Could not find git clone done message.")

    runningSetup = False 

    if runningSetup:
        # Find cell 2, where we can enter the setup command
        cssSel = "div.cell:nth-child(2) > div:nth-child(1) > div:nth-child(2) > div:nth-child(2) > div:nth-child(1) > div:nth-child(1) > textarea:nth-child(1)"
        WebDriverWait(browser,10).until(
                EC.presence_of_element_located((By.CSS_SELECTOR, cssSel)))
        code_input = browser.find_element_by_css_selector(cssSel)
        ActionChains(browser).move_to_element(code_input).click().send_keys("!workshopai/src/setup-wrkshp").key_down(Keys.SHIFT).send_keys(Keys.ENTER).key_up(Keys.SHIFT).perform()
        # find the output box for that command
        cssSel = "div.cell:nth-child(2) > div:nth-child(2) > div:nth-child(2) > div:nth-child(1) > div:nth-child(2) > pre:nth-child(1)"
        WebDriverWait(browser,10).until(
                EC.presence_of_element_located((By.CSS_SELECTOR, cssSel)))
        waitforStringInCSSSel(browser, cssSel, "Done with setup", 20)

        log.info(f"Finished setup of environment for user {args.u}")

    browser.switch_to_window(JNWinHandle)

    # navigate to the notebook file

    oldWindowHandles = browser.window_handles
    # We need to refresh here, as jupyter may not have seen the change yet.
    browser.refresh()

    for link in ["workshopai", "notebooks", "python_intro_bootstrap"]:
        WebDriverWait(browser, 20).until(
            EC.presence_of_element_located((By.PARTIAL_LINK_TEXT, link)))
        browser.find_element_by_partial_link_text(link).click()

    time.sleep(1)
    # switch to the new notebook tab
    switchToNewWindowHandle(browser, oldWindowHandles)

    time.sleep(2)

    log.debug(f"browser.title is {browser.title}")

    # try to find the top cell
    topCellSel = "h1"

    WebDriverWait(browser,10).until(
            EC.presence_of_element_located((By.CSS_SELECTOR, topCellSel)))

    topCell = browser.find_element_by_css_selector(topCellSel)
    log.debug(f"topCell.text is {topCell.text}, user {args.u}")
    assert("Bootstrapping a Truckload" in topCell.text)

    if (args.s):
            # take a screenshot of the success
            browser.get_screenshot_as_file(f"notebook_u{args.u}_{args.t}.png")

    # Let's now try to run the whole notebook.

    kernelMenu = browser.find_element_by_css_selector("li.dropdown:nth-child(6) > a:nth-child(1)")
    kernelMenu.click()

    log.debug("clicked kernelMenu")

    restartRunAllButton = browser.find_element_by_css_selector("#restart_run_all > a:nth-child(1)")
    restartRunAllButton.click()

    log.debug("clicked RestartRunAll")
    time.sleep(1)

    confirmButtonSel = "button.btn-sm:nth-child(2)"
    try: 
        WebDriverWait(browser,10).until(
            EC.presence_of_element_located((By.CSS_SELECTOR, confirmButtonSel)))
    except selenium.common.exceptions.TimeoutException as e:
            log.error(f"Got exception {e}, type {type(e)} but ignoring and continuing")
    else:
        browser.find_element_by_css_selector(confirmButtonSel).click()

    time.sleep(3)

    # For Keri's bootstrapping notebook, test to make sure that numpy loaded OK and we see
    # the types.

    wantLabelStr = "In [3]:"
    log.debug(f"Waiting for \"{wantLabelStr}\" in Keri's notebook, user {args.u}")
    labelSel = "div.cell:nth-child(5) > div:nth-child(1) > div:nth-child(1)"
    waitforStringInCSSSel(browser, labelSel, wantLabelStr, 600)

    # Do we see the output from her script?

    cell3output = False

    cell3output = browser.find_element_by_css_selector("div.cell:nth-child(5) > div:nth-child(2) > div:nth-child(2) > div:nth-child(1)")

    log.debug(f"Found the following in cell 3 output: {cell3output.text}")

    # the output in this cell varies, so let's make an RE that should match

    e = re.compile(r'\[\s*\d+(\s+\d+){2,2}')
    result = e.search(cell3output.text)
    log.debug(f"testing against RE {e}, result is {result}")

    assert( e.search(cell3output.text) )

    # Go back to the first JN tab

    browser.switch_to_window(JNWinHandle)

    # Pick the Notebook 1 (Rectangle Classifier) notebook

    oldWindowHandles = browser.window_handles
    browser.find_element_by_partial_link_text("RectangleClassifier01").click()
    WebDriverWait(browser, 20).until(
        EC.new_window_is_opened(oldWindowHandles))
    switchToNewWindowHandle(browser, oldWindowHandles)
    WebDriverWait(browser,40).until(
        EC.presence_of_element_located((By.CSS_SELECTOR, "div.CodeMirror.cm-s-ipython")))

    log.debug(f"browser.title is {browser.title}")

    assert("Rectangle" in browser.title)

    # Check to make sure that python modules (esp. tensorflow) loaded OK
    # Find the left cell label which should eventually be "In [2]:"

    wantLabelStr = "In [2]:"
    log.debug(f"Waiting for \"{wantLabelStr}\", user {args.u}")
    labelSel = "div.cell:nth-child(5) > div:nth-child(1) > div:nth-child(1) > div:nth-child(1)"

    waitforStringInCSSSel(browser, labelSel, wantLabelStr, 600)

    cell2output = False
    try:
        cell2output = browser.find_element_by_css_selector(".output_text > pre:nth-child(1)")
    except selenium.common.exceptions.WebDriverException:
        # no problem if it isn't there
        pass

    if (cell2output and "ModuleNotFoundError" in cell2output.text):
        log.error(f"user {args.u} Found ModuleNotFoundError in cell 2.  It contains: {cell2output.text}")
        raise Exception("ModuleNotFoundError in notebook.")

    if (args.s):
        # Get another screenshot
        time.sleep(1)
        browser.get_screenshot_as_file(f"notebook_plot_u{args.u}_{args.t}.png")

    # Now, switch back to the "interactive sessions" tab
    browser.switch_to_window(iaWindowHandle)

    # Find the "delete" button.

    deleteBtn = browser.find_element_by_css_selector("a.btn:nth-child(1)")
    deleteBtn.click()

    # Wait for "confirmed" button

    confirmButSel = ".commit"
    WebDriverWait(browser,20).until(
            EC.presence_of_element_located((By.CSS_SELECTOR, confirmButSel)))
    browser.find_element_by_css_selector(confirmButSel).click()

    # Wait for "successfully deleted"
    successSel = ".alert"
    WebDriverWait(browser,10).until(
            EC.presence_of_element_located((By.CSS_SELECTOR, successSel)))
    text = browser.find_element_by_css_selector(successSel).text
    assert("successfully deleted" in text)

    totalTime = time.monotonic() - startMonoTime
    log.info(f"Successfully completed test for user {args.u}. Total time: {totalTime} seconds.")

browser = webdriver.Firefox()

try:
    notebookTest(browser, username, password, args)
except selenium.common.exceptions.WebDriverException as wde:
    log.error(f"Some exception from the web driver while running for user {args.u}: {wde} More details on exception for user {args.u}: { traceback.format_list(traceback.extract_tb(sys.exc_info()[2]))} ")
    if (args.s):
        browser.get_screenshot_as_file(f"exception_{args.u}_{args.t}.png")
except TimeoutException as e:
    log.error(f"TimeoutException for user {args.u}: {e}")
    if (args.s):
        browser.get_screenshot_as_file(f"tmoutex_{args.u}_{args.t}.png")
except Exception as e:
    log.error(f"Some other exception I did not expect while running for user {args.u}: {e}")
    traceback.print_tb(sys.exc_info()[2])
finally:
    time.sleep(5)
    browser.quit()


# vim:ts=4:sw=4
