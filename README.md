Here's the README for the vacc-tests repo.

using dogfish as gitlab-runner host:
----
I've got .gitlab-runner configured this way:


```
[[runners]]
  name = "dogfish-ssh-to-dg-user1"
  url = "https://gitlab.uvm.edu/"
  token = [REDACTED]
  executor = "ssh"
  [runners.custom_build_dir]
  [runners.ssh]
    user = "kapoodle"
    host = "localhost"
    port = "20058"
    identity_file = "/users/k/a/kapoodle/.ssh/id_rsa"
```

This is because as of this writing, you can't ssh directly to dg-user1.  Note
that this requires an SSH port forwarder running at port 20058 in addition
to the normal `gitlab-runner run` run by kapoodle.

```
ssh -N -L 20058:dg-user1:22 kapoodle@bluemoon-user1.uvm.edu
```



